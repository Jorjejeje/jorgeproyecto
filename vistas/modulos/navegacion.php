<nav class="navegador">
	<a href="index.php"><img src="img/logo.svg" alt="logo" id="logo" /></a>
	<ul>
		<!-- Document 06 - MVC ...action te lleva a ...-->
		<li onclick="Menu()"><a href="index.php">Inicio</a></li>
		<li onclick="Menu()"><a href="index.php?action=nosotros">Nosotros</a></li>
		<li onclick="Menu()"><a href="index.php?action=servicios">Servicios</a></li>
		<li onclick="Menu()"><a href="index.php?action=galeria">Galería</a></li>
		<li onclick="Menu()"><a href="index.php?action=contactenos">Contáctenos</a></li>
		<li onclick="Menu()" id="redesSociales">
			<a href="https://www.facebook.com"><i class="fab fa-facebook-square"></i></a>
			<a href="">
				<i class="fab fa-instagram-square"></i>
			</a>
			<a href=""><i class="fab fa-whatsapp-square"></i></a>
			<a href=""><i class="fab fa-youtube"></i></a>

		</li>
	</ul>
	<i class="fas fa-align-justify" id="toggle" onclick="Menu()"></i>
</nav>