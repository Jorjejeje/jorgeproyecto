<section class="paginaNosotros">
    <h1>Nosotros</h1>
    <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae voluptas consequatur, maxime possimus, magni assumenda natus tenetur, sint odit totam tempora? Quidem et doloribus, delectus esse eos cum soluta perspiciatis.lorem Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam in eveniet, voluptate perspiciatis, saepe alias blanditiis maxime consectetur animi, similique accusamus minima. Minima veniam hic magni eaque mollitia dignissimos culpa! Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro nulla consequuntur esse a asperiores? Quas natus quaerat maiores, nobis a perferendis doloremque! Vel, atque sed. Corporis eveniet est voluptate illum?
    </p>
    <div class="mision">
        <span>Mision</span>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque omnis est voluptatem minus porro repellat ipsam culpa! Officia nemo recusandae ipsa sed consequuntur doloremque amet id, alias, nobis velit iste?</p>
        <img src="img/c1.jpg" />

    </div>
    <div class="vision">
        <span>Vision</span>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque omnis est voluptatem minus porro repellat ipsam culpa! Officia nemo recusandae ipsa sed consequuntur doloremque amet id, alias, nobis velit iste?</p>
        <img src="img/c2.jpg" />

    </div>

    <div class="empleados">
        <div class="empleado">
            <span>Nombre</span>
            <img src="img/profile.png"/>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi molestiae repudiandae rerum! Ad itaque ab deleniti accusamus odit ullam? Quos totam facere autem quidem tempore quibusdam. Corrupti dolorem eos quo?</p>
        </div>
        <div class="empleado">
            <span>Nombre</span>
            <img src="img/profile.png"/>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi molestiae repudiandae rerum! Ad itaque ab deleniti accusamus odit ullam? Quos totam facere autem quidem tempore quibusdam. Corrupti dolorem eos quo?</p>
        </div>
        <div class="empleado">
            <span>Nombre</span>
            <img src="img/profile.png"/>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi molestiae repudiandae rerum! Ad itaque ab deleniti accusamus odit ullam? Quos totam facere autem quidem tempore quibusdam. Corrupti dolorem eos quo?</p>
        </div>
        <div class="empleado">
            <span>Nombre</span>
            <img src="img/profile.png"/>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi molestiae repudiandae rerum! Ad itaque ab deleniti accusamus odit ullam? Quos totam facere autem quidem tempore quibusdam. Corrupti dolorem eos quo?</p>
        </div>
        <div class="empleado">
            <span>Nombre</span>
            <img src="img/profile.png"/>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi molestiae repudiandae rerum! Ad itaque ab deleniti accusamus odit ullam? Quos totam facere autem quidem tempore quibusdam. Corrupti dolorem eos quo?</p>
        </div>
    </div>
</section>