<section class="paginaContactenos">
	<h1>Contáctenos</h1>
	<div class="mapa">
		<label>AQUI IRIA EL MAPA</label>
	</div>
	<form id="contacto" method="POST" action="formulario.php">
		<div class="elementoForm">
			<label>Nombre</label>
			<input type="text" name="nombre" />
		</div>
		<div class="elementoForm">
			<label>E-mail</label>
			<input type="email" name="email" />
		</div>
		<div class="elementoForm">
			<label>Telefono</label>
			<input type="tel" name="telefono" />
		</div>
		<div class="elementoForm">
			<label>Comentarios</label>
			<textarea name="comentarios" id="comentarios" cols="30" rows="10"></textarea>
		</div>
		<input type="submit" name="submit" value="Enviar"/>
	</form>
	<div class="MasInfo">
		<span id="direccion">Direccion: Lorem, ipsum dolor sit amet consectetur adipisicing elit. Architecto quia nam autem quas nisi veritatis iste sapiente sequi, eius accusamus maxime. Sapiente, atque! Quidem esse distinctio totam quaerat accusamus. Et!</span>
		<span>Email: stefmileswebdev@gmail.com</span>
		<span>Teléfono: +50374893644</span>
	</div>
</section>