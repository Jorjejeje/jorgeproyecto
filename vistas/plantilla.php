<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	

	<link rel="stylesheet" type="text/css" href="css/plantilla.css" />
	<link rel="stylesheet" type="text/css" href="css/navegacion.css" />
	<link rel="stylesheet" type="text/css" href="css/inicio.css" />
	<link rel="stylesheet" type="text/css" href="css/nosotros.css" />
	<link rel="stylesheet" type="text/css" href="css/servicios.css" />
	<link rel="stylesheet" type="text/css" href="css/contactenos.css" />
	<link rel="stylesheet" type="text/css" href="css/galeria.css" />
	<link rel="stylesheet" type="text/css" href="css/container.css" />
	<link rel="stylesheet" type="text/css" href="css/parrafo.css" />

	<!-- jquery -->
	<script src="js/nav.js"></script>

	<!-- botonesRedes -->
	<script src="https://kit.fontawesome.com/ab36d0f00e.js" crossorigin="anonymous"></script>

	<title>Documento MVC</title>
</head>

<body>

	<!-- Zona header-->
	<header>

		<!-- Zona nav: Menu navegación -->
		<!-- esto es la botonera...apache carga lo que hay en el modulo -->
		<?php
		include "modulos/navegacion.php";
		?>

		<!-- direccionamiento absoluta  si cambias el nombre del servidor da problema-->
		<!-- 	<img src="http://localhost/jorge/img/landscape1.jpg"> -->

	</header>

	<!-- Zona Contenido -->


	<main>








		<?php

		$mvc = new MvcController();
		$mvc->enlacesPaginasController();

		?>



	</main>

</body>

</html>