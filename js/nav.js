//const=constante; var=variable; let=variable; cambiar clases css...
const Menu=()=>{
	//querySelector TE PERMITE OBTENER UNA ETIQUETA DE HTML A TRAVES DE SU selector css
    ul=document.querySelector("ul");
    /* ya obtenida la etiqueta ul se accede a todas 
    sus clases con classlist ("array que guarda las clases")
    togle=aplica una condición, 1ero revisa si es ul tiene esa clase activa
    si ya tiene la clase active, la elimina; si no la tiene la agrega*/
    ul.classList.toggle("active");
    /* ya obtenida la etiqueta i del menu hamburguesa accede a todas 
    sus clases con classlist ("array que guarda las clases")
    togle=aplica una condición, 1ero revisa si es ul tiene esa clase activa
    si ya tiene la clase active, la elimina; si no la tiene la agrega*/
    i=document.querySelector("#toggle");
    i.classList.toggle("active")
}