<?php

class MvcController{

	#LLAMADA A LA PLANTILLA
	#------------------------------------------------
	public function plantilla(){
		
		include "vistas/plantilla.php";
	}

	#INTERACCIÓN DEL USUARIO
	#-----------------------------------------------
	public function enlacesPaginasController(){ 

// Documento 06 - MVC
// isset es para decirle que la página trae contenido

		if(isset($_GET["action"])) {
			$enlacesController = $_GET["action"];
		}
// Document 06 - MVC
// Si ve buit amb else, li dire que $enlacesController = index
		else{
			$enlacesController = "index";
		}

		// hacemos una llamada al metodo statico enlacesPaginasModel, que da una accion (pagina),
		// nos devuelve su plantilla php , etc.
		$respuesta = EnlacesPaginas::enlacesPaginasModel($enlacesController);

		include $respuesta;	
	}
}

?>